/**
 * @author Kamil Szymanski
 * @version 1.0
 * @since 01.05.2022
 */

import java.util.Scanner;

public class RaumschiffTest {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		
		Raumschiff k = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Heghta" );
		Raumschiff r = new Raumschiff(2, 100, 100, 100, 100, 2, "IKS Khazara" );
		Raumschiff v = new Raumschiff(0, 80, 80, 50, 100, 5,  "NiVar" );
		
		Ladung k1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung k2 = new Ladung("Klingonen Schwert", 200);
		
		Ladung r1 = new Ladung("Borg Schrott", 2);
		Ladung r2 = new Ladung("rote Materie", 200);
		Ladung r3 = new Ladung("Plasma Waffen", 50);
		
		Ladung v1 = new Ladung("Forschungs Sonde", 35);
		Ladung v2 = new Ladung("Photonen Torpedo", 3);
		
		k.setPhotonentorpedoAnzahl(1); //Testwert = 300
		k.setEnergieversorgungInProzen(100);
		k.setSchildeInProzent(100);
		k.setHuelleInProzent(100);
		k.setLebenserhaltungssystemeInProzent(100);
		k.setSchiffsname("IKS Heghta");
		k.setAndroidenAnzahl(2);
		
		r.setPhotonentorpedoAnzahl(2);
		r.setEnergieversorgungInProzen(100);
		r.setSchildeInProzent(100);
		r.setHuelleInProzent(100);
		r.setLebenserhaltungssystemeInProzent(100);
		r.setSchiffsname("IRW Khazara");
		r.setAndroidenAnzahl(2);
		
		v.setPhotonentorpedoAnzahl(0); //Testwert = 300
		v.setEnergieversorgungInProzen(80);
		v.setSchildeInProzent(80);
		v.setHuelleInProzent(50);
		v.setLebenserhaltungssystemeInProzent(100);
		v.setSchiffsname("NiVar");
		v.setAndroidenAnzahl(5);
	
		k1.setBezeichnung("Ferengi Schneckensaft");
		k1.setMenge(200);
		
		k2.setBezeichnung("Klingonen Schwert");
		k2.setMenge(200);

		r1.setBezeichnung("Borg Schrott");
		r1.setMenge(2);
		
		r2.setBezeichnung("rote Materie");
		r2.setMenge(2);
		
		r3.setBezeichnung("Plasma Waffen");
		r3.setMenge(50);

		v1.setBezeichnung("Forschungs Sonde");
		v1.setMenge(35);
		
		v2.setBezeichnung("Photonen Torpedo");
		v2.setMenge(3);	

		System.out.println(k.ladungsverzeichnis());
		
		k.addLadung(k1);
		
	}
	
	
} //Ende MainRaumschiff





/**
 * @author Kamil Szymanski
 * @version 1.0
 * @since 01.05.2022
 */
public class Ladung {
	
	public int menge;
	public String bezeichnung;
	


public Ladung() {
} 


public Ladung(String bezeichnung, int menge) {

	this.menge = menge;
	this.bezeichnung = bezeichnung;
}

/**
 * @return int 
 */
public int getMenge() {
	return menge;
}
/**
 * @param menge
 */

public void setMenge(int menge) {
	this.menge = menge;
}

/**
 * @return String 
 */

public String getBezeichnung() {
	return bezeichnung;
}

/**
 * @param bezeichnung 
 */

public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}

}



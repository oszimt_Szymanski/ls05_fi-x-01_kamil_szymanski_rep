/**
 * @author Kamil Szymanski
 * @version 1.0
 * @since 01.05.2022
 */

import java.util.ArrayList;

public class Raumschiff {

		private int photonentorpedoAnzahl;
		private int energieversorgungInProzen;
		private int schildeInProzent;
		private int huelleInProzent;
		private int lebenserhaltungssystemeInProzent;
		private int androidenAnzahl;
		private String schiffsname;
		private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
		private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

		// Ende Raumschiff

		public Raumschiff() {
		}

		public Raumschiff(int photonentorpedoAnzahl, 
				int energieversorgungInProzen, 
				int schildeInProzent,
				int huelleInProzent, 
				int lebenserhaltungssystemeInProzent, 
				int androidenAnzahl, 
				String schiffsname
				) {

			
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
			this.energieversorgungInProzen = energieversorgungInProzen;
			this.schildeInProzent = schildeInProzent;
			this.huelleInProzent = huelleInProzent;
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
			this.androidenAnzahl = androidenAnzahl;
			this.schiffsname = schiffsname;
//			this.broadcastKommunikator = broadcastKommunikator;
//			this.ladungsverzeichnis = ladungsverzeichnis;
			
		} // Ende Raumschiffkonstruktor
/**
 * @param photonentorpedoAnzahl
 */
		public void setphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
/**
 * @return int
 */
		public void getphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
/**
 * @return int
 */
		public int getPhotonentorpedoAnzahl() {
			return photonentorpedoAnzahl;
		}
/**
 * @param photonentorpedoAnzahl
 */
		public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
/**
 * @return int
 */
		public int getEnergieversorgungInProzen() {
			return energieversorgungInProzen;
		}
/**
 * @param energieversorgungInProzen
 */
		public void setEnergieversorgungInProzen(int energieversorgungInProzen) {
			this.energieversorgungInProzen = energieversorgungInProzen;
		}
/**
 * @return int
 */
		public int getSchildeInProzent() {
			return schildeInProzent;
		}
/**
 *  @param schildeInProzent
 */
		public void setSchildeInProzent(int schildeInProzent) {
			this.schildeInProzent = schildeInProzent;
		}
/**
 * @return int
 */
		public int getHuelleInProzent() {
			return huelleInProzent;
		}
/**
 * @param huelleInProzent
 */
		public void setHuelleInProzent(int huelleInProzent) {
			this.huelleInProzent = huelleInProzent;
		}
/**
 * @return int
 */
		public int getLebenserhaltungssystemeInProzent() {
			return lebenserhaltungssystemeInProzent;
		}
/**
 *  @param lebenserhaltungssystemeInProzent
 */
		public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		}
/**
 * @return int 
 */
		public int getAndroidenAnzahl() {
			return androidenAnzahl;
		}
/**
 * @param androidenAnzahl
 */
		public void setAndroidenAnzahl(int androidenAnzahl) {
			this.androidenAnzahl = androidenAnzahl;
		}
/**
 *  @return int 
 */
		public String getSchiffsname() {
			return schiffsname;
		}
/**
 * @param Schiffsname
 */
		public void setSchiffsname(String schiffsname) {
			this.schiffsname = schiffsname;
		}
/**
 * @return Arraylist 
 */
		public ArrayList getbroadcastKommunikator(ArrayList broadcastKommunikator) {
			return broadcastKommunikator;
		}
/**
 * @param broadcastKommunikator
 */
		public void setbroadcastKommunikator(ArrayList broadcastKommunikator) {
			this.broadcastKommunikator = broadcastKommunikator;
		}
/**
 * @return int 
 */
		public ArrayList getladungsverzeichnis(ArrayList ladungsverzeichnis) {
			return ladungsverzeichnis;
		}
/**
 *  @param ladungsverzeichnis
 */
		public void setladungsverzeichnis(ArrayList ladungsverzeichnis) {
			this.ladungsverzeichnis = ladungsverzeichnis;
		}

		// Methoden

		public void photonentorpedoSchiessen(Raumschiff r) {
			System.out.println("Feuer");
		} // schiessen

		public void phaserkanoneSchiessen(Raumschiff r) {
			System.out.println("Feuer Phaser");
		} // schiessen Phaser

		private void treffer(Raumschiff r) {
			System.out.println("Wir wurden getroffen");
		} // Treffer Anzahl

		public void zustandRaumschiff(Raumschiff r) {
			System.out.println("Schiff besch�digt");
		} // Zustand Schiff

		public void nachrichtAnAlle(String message) {
			System.out.println("Nachricht an Alle");
		} // Nachricht
		public void addLadung(Ladung Ladung) {
			this.ladungsverzeichnis.add(Ladung);
			
		}
	}
	
	


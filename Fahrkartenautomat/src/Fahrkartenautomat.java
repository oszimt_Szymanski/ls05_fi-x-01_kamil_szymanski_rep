﻿//import java.util.Scanner;
//
//class Fahrkartenautomat
//{
//    public static void main(String[] args)
//    {
//       Scanner tastatur = new Scanner(System.in);
//      
//       double zuZahlenderBetrag; 
//       double eingezahlterGesamtbetrag;
//       double eingeworfeneMuenze;
//       double rueckgabebetrag;
//
//       System.out.print("Zu zahlender Betrag (EURO): ");
//       zuZahlenderBetrag = tastatur.nextDouble();
//
//       // Geldeinwurf
//       // -----------
//       eingezahlterGesamtbetrag = 0.0;
//       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
//       {
//    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
//    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
//    	   eingeworfeneMuenze = tastatur.nextDouble();
//           eingezahlterGesamtbetrag += eingeworfeneMuenze;
//       }
//
//       // Fahrscheinausgabe
//       // -----------------
//       System.out.println("\nFahrschein wird ausgegeben");
//       for (int i = 0; i < 8; i++)
//       {
//          System.out.print("=");
//          try {
//			Thread.sleep(250);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//       }
//       System.out.println("\n\n");
//
//       // Rueckgeldberechnung und -Ausgabe
//       // -------------------------------
//       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
//       if(rueckgabebetrag > 0.0)
//       {
//    	   System.out.println("Der Rueckgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
//    	   System.out.println("wird in folgenden Muenzen ausgezahlt:");
//
//           while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
//           {
//        	  System.out.println("2 EURO");
//	          rueckgabebetrag -= 2.0;
//           }
//           while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
//           {
//        	  System.out.println("1 EURO");
//	          rueckgabebetrag -= 1.0;
//           }
//           while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
//           {
//        	  System.out.println("50 CENT");
//	          rueckgabebetrag -= 0.5;
//           }
//           while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
//           {
//        	  System.out.println("20 CENT");
// 	          rueckgabebetrag -= 0.2;
//           }
//           while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
//           {
//        	  System.out.println("10 CENT");
//	          rueckgabebetrag -= 0.1;
//           }
//           while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
//           {
//        	  System.out.println("5 CENT");
// 	          rueckgabebetrag -= 0.05;
//           }
//       }
//
//       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
//                          "vor Fahrtantritt entwerten zu lassen!\n"+
//                          "Wir wünschen Ihnen eine gute Fahrt.");
//    }
//    
//    public static int rueckgeldAusgeben(String text);
//}
//import java.util.Scanner;
//
//class Fahrkartenautomat
//{
//    public static void main(String[] args)
//    {
//       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
//       double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
//       fahrkartenAusgeben();
//       rueckgeldAusgeben(rueckgabebetrag);
//
//       System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
//                          "vor Fahrtantritt entwerten zu lassen!\n"+
//                          "Wir wünschen Ihnen eine gute Fahrt.");
//    }
//    
//    public static double fahrkartenbestellungErfassen() {
//    	Scanner tastatur = new Scanner(System.in);
//    	
//    	System.out.print("Wie viel kostet ein Ticket? ");
//        double ticketEinzelpreis = tastatur.nextDouble();
//        
//        System.out.print("Wie viele Tickets moechten Sie kaufen? ");
//        double anzahlTickets = tastatur.nextInt();
//    	
//    	return ticketEinzelpreis * anzahlTickets;
//    }
//    
//    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
//    	Scanner tastatur = new Scanner(System.in);
//    	
//    	double eingezahlterGesamtbetrag = 0.0;
//        double eingeworfeneMuenze;
//        
//    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
//        {
//     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
//     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
//     	   System.out.print("\nEingabe (mind. 5Ct, hoechstens 2 Euro): ");
//     	   eingeworfeneMuenze = tastatur.nextDouble();
//           eingezahlterGesamtbetrag += eingeworfeneMuenze;
//        }
//    	
//    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
//    }
//    
//    public static void fahrkartenAusgeben() {
//    	System.out.println("\nFahrscheine werden ausgegeben.");
//        for (int i = 0; i < 8; i++)
//        {
//           System.out.print("=");
//           try {
// 			Thread.sleep(250);
// 		} catch (InterruptedException e) {
// 			// TODO Auto-generated catch block
// 			e.printStackTrace();
// 		}
//        }
//        System.out.println("\n\n");
//    }
//    
//    public static void rueckgeldAusgeben(double rueckgabebetrag) {
//    	if(rueckgabebetrag > 0.0)
//        {
//     	   System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
//     	   System.out.println("wird in folgenden Muenzen ausgezahlt:");
//
//            while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
//            {
//         	  System.out.println("2 EURO");
// 	          rueckgabebetrag -= 2.0;
//            }
//            while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
//            {
//         	  System.out.println("1 EURO");
// 	          rueckgabebetrag -= 1.0;
//            }
//            while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
//            {
//         	  System.out.println("50 CENT");
// 	          rueckgabebetrag -= 0.5;
//            }
//            while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
//            {
//         	  System.out.println("20 CENT");
//  	          rueckgabebetrag -= 0.2;
//            }
//            while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
//            {
//         	  System.out.println("10 CENT");
// 	          rueckgabebetrag -= 0.1;
//            }
//            while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
//            {
//         	  System.out.println("5 CENT");
//  	          rueckgabebetrag -= 0.05;
//            }
//        }
//    }
//}

import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(rueckgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);

        double ticketEinzelpreis = 1.0;
        boolean running = false;

        do {
        	System.out.print("Bitte wählen Sie eine der folgenden Ticketarten:\n\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
        	int auswahl = tastatur.nextInt();
        	
        	switch (auswahl) {
            case 1: ticketEinzelpreis = 2.9; running = false; break;
            case 2: ticketEinzelpreis = 8.6; running = false; break;
            case 3: ticketEinzelpreis = 23.5; running = false; break;
            default: 
            	System.out.println("\nGeben Sie eine Ganzzahl zwischen 1 und 3 ein.\n");
            	running = true;
            	break;
            }
        } while (running);
        
        System.out.print("Wie viele Tickets werden gekauft?: ");
        double anzahlTickets = tastatur.nextInt();
    	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double rueckgabebetrag, String einheit) {
    	while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 " + einheit);
	          rueckgabebetrag -= 2.0;
        }
        while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 " + einheit);
	          rueckgabebetrag -= 1.0;
        }
        while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("0.50 " + einheit);
	          rueckgabebetrag -= 0.5;
        }
        while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("0.20 " + einheit);
	          rueckgabebetrag -= 0.2;
        }
        while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("0.10 " + einheit);
	          rueckgabebetrag -= 0.1;
        }
        while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("0.05 " + einheit);
	          rueckgabebetrag -= 0.05;
        }
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
    	if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rueckgabebetrag);
     	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
     	   muenzeAusgeben(rueckgabebetrag, " EURO");
        }
    }
}

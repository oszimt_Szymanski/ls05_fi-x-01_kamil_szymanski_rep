import java.lang.reflect.Array;
import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzen;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	// Ende Raumschiff

	public Raumschiff() {
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzen, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList broadcastKommunikator, ArrayList ladungsverzeichnis) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzen = energieversorgungInProzen;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;

	} // Ende Raumschiffkonstruktor

	public void setphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public void getphotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzen() {
		return energieversorgungInProzen;
	}

	public void setEnergieversorgungInProzen(int energieversorgungInProzen) {
		this.energieversorgungInProzen = energieversorgungInProzen;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList getbroadcastKommunikator(ArrayList broadcastKommunikator) {
		return broadcastKommunikator;
	}

	public void setbroadcastKommunikator(ArrayList broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList getladungsverzeichnis(ArrayList ladungsverzeichnis) {
		return ladungsverzeichnis;
	}

	public void setladungsverzeichnis(ArrayList ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	// Methoden

	public void photonentorpedoSchiessen(Raumschiff r) {
		System.out.println("Feuer");
	} // schiessen

	public void phaserkanoneSchiessen(Raumschiff r) {
		System.out.println("Feuer Phaser");
	} // schiessen Phaser

	private void treffer(Raumschiff r) {
		System.out.println("Wir wurden getroffen");
	} // Treffer Anzahl

	public void zustandRaumschiff(Raumschiff r) {
		System.out.println("Schiff besch�digt");
	} // Zustand Schiff

	public void nachrichtAnAlle(String message) {
		System.out.println("Nachricht an Alle");
	} // Nacht 
}
	



import java.util.Scanner;


public class Konsoleneingabe
{ 
	 
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie mir Ihren Namen an: ");    
     
    // Die Variable name1 speichert die erste Eingabe 
    String name1 = myScanner.next();  
     
    System.out.print("Bitte geben Sie Ihr Alter ein: "); 
     
    // Die Variable age1 speichert die zweite Eingabe 
    int age1 = myScanner.nextInt();  
     
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(name1 + " " + age1 + "\n" );   
 
    myScanner.close(); 
     
   
  }    
} 
